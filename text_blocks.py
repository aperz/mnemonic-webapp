introduction = '''
    <h2>MetageNomic Experiment Mining to create an OTU Network of Inhabitant Correlations</h2>
    <p>
    This section is an intro to the project.
    </p>
    <p>
    The presented tool downloads metagenomic data available on EBI
    Metagenomics – currently almost 1000 projects and almost 60000
    samples – and aims at providing data-based insight into new
    metagenomic experiments, as well as providing a framework for the
    exploration of existing data.
    </p>
    <p>
    Finding samples and experiments that are similar to query samples
    allows the potential discovery of new properties of the finding and
    possibly relevant literature. A keyword-based approach has been
    applied to classify the data, as the metadata is very often inconsistent,
    insufficient, or simply missing across experiments. This enables query
    samples to be classified for further analysis. Inference of subgroups
    within experiments is accomplished with text analysis. Analysis of
    differential abundance between those groups enables an analysis of
    microbe-condition and microbe-microbe relationships.
    </p>
    '''

help_page = """
    <h2>MNEMONIC help page</h2>
    <h3>TF-IDF tranformation</h3>
    <p>
    First, the abundance matrix of taxa versus samples is subject to TF-IDF transformation
    (term frequency - inverse document frequency), which allows to weight up a taxonomic unit
    that is rare across samples.
    <br>
    <b>IDF</b> - inverse of number of samples that contain taxon in question<br>
    <b>TF</b> - term frequency in sample in question
    <br>
    The values may be subject to sublinear scaling (1+log(tf)).
    <br>
    Read more here: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfTransformer.html
    </p>
    <h3>Cosine distance</h3>
    <p>
    Measure of similarity that is a cosine of the angle between two vectors of an inner product space.
    For vectors whise values are only non-negative (such as taxonomic counts) cosine distance is bound
    between 0 and 1 because the angle cannot be greater than 90.
    If vectors are mean-centered, it's equivalent to Pearson correlation coefficient.
    </p>
    <h3>Differential analysis</h3>
    <p>
    Meaning of the differential analysis results, as per DESeq2 Beginner's guide:
    </p>
    <p>
    <b>baseMean        </b>:                      the base mean over all rows<br>
    <b>log2FoldChange  </b>:        log2 fold change (MAP): treatment vs Control<br>
    <b>lfcSE           </b>:                standard error: treatment vs Control<br>
    <b>stat            </b>:                Wald statistic: treatment vs Control<br>
    <b>pvalue          </b>:             Wald test p-value: treatment vs Control<br>
    <b>padj            </b>:                                BH adjusted p-values<br>
    </p>
    """
