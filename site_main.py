#!/usr/bin/env python3

'''
the more stats the better
show don't tell

- metric for similar projects (print and sort)
- entity pages: projects, samples, biomes etc:
    - stats: most abundant taxa
    - average distance to the average of each biome
    - link to EBI page for that entity
    -* links to similar entities?
- precalculate cache (decrease response time)
- plt.close('all'): remove this workaround and use the 'Agg' backend properly
#import matplotlib
#matplotlib.use('Agg')

AESTHETIC
=========

TODO
- [x] download tables button
- [x] redirect even if query matches only one taxon
- [ ] sample page throws a linkage warning
- [ ] BUG: change diff analysis file names "---" instead of "-", because some biome names contain '-' (Host-associated)



'''

from isdev import ISDEV
if ISDEV:
    TITLE = "MNEMONIC (how about mnome / mnemo)"
    #ATT
    #PORT    = 5000
    PORT    = 9001
    HOST    = '0.0.0.0'
    SUFFIX  = "DEVEL"
    MAX_ROW = 10
    ABN_FP = '/P/mnemonic-webapp-dev/data/dummy_A.tsv'
else:
    TITLE = "MNEMONIC"
    PORT    = 9000
    HOST    = '0.0.0.0'
    SUFFIX  = ""
    MAX_ROW = 100
    ABN_FP = '/D/ebi/A_OTU-TSV_v2_intestine.tsv'


import matplotlib
matplotlib.use('Agg')

from flask import *
import pandas as pd
import re
import base64
from io import StringIO, BytesIO
import datetime
import inspect
from scipy.cluster.hierarchy import dendrogram

import matplotlib.pyplot as plt
import seaborn as sns
import itertools


import text_blocks as TEXT
from skrzynka import query_rows, delete_empty, names_to_human_readable, merge_data_frames, pluralize
from pairwise import pairwise_to_long, collapse_pairwise
import tool_similar_projects as tl
#from metadata_samples import has_keyword
from plots import *
from data_management import *
import metadifferential as mdiff

app = Flask(__name__)

# tables
pd.set_option('display.max_colwidth', 1000)

# cache
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)


figdir = 'figures/'
#USE_DUMMY   = ISDEV

REDIR_HOME = "home"
REDIR_INTRO = "intro"
FILE_STORAGE = "file_storage"

if not os.path.isdir(FILE_STORAGE):
    os.mkdir(FILE_STORAGE)



URL_TEMPLATES = {
    'ebi_projects':         'https://www.ebi.ac.uk/metagenomics/projects/{0}',
    'ebi_samples':          'https://www.ebi.ac.uk/metagenomics/samples/{0}',
    'ebi_projects_search':  'https://www.ebi.ac.uk/metagenomics/projects/doSearch?searchTerm={0}&biome=ALL&search=Search',
    'ebi_samples_search':   'https://www.ebi.ac.uk/metagenomics/samples/doSearch?searchTerm={0}&biome=ALL&search=Search',
    'mnemo_samples':        '/samples/{0}',
    'mnemo_projects':       '/projects/{0}',
    'mnemo_biomes':         '/biomes/{0}',
    'mnemo_taxa':           '/taxa/{0}',
    'mnemo_families':       '/families/{0}',
    'mnemo_contrasts':      '/contrasts/{0}',
    'mnemo_metacontrasts':      '/metacontrasts/{0}',
}

# black as placeholder
COLOURS = {
    'sample':               'green',
    'project':              'blue',
    'taxon':                'red',
    'biome':                'yellow',
    'function':             'orange',
    'condition':            'purple',
}

ENTITY_LABELS_SAM = ['project', 'sample', 'biome', 'condition', 'keyword']
ENTITY_LABELS_TAX = ['taxonomy', 'family', 'order', 'phylum', 'class']


# not needed?
def _render_template(template_name_or_list, **kwargs):
    return render_template(template_name_or_list = template_name_or_list,
                    SUFFIX=SUFFIX, REDIR_INTRO = REDIR_INTRO,
                    TITLE = TITLE,
                    REDIR_HOME = REDIR_HOME,
                    **kwargs
                    )
    # TODO store globals in a dict

def _render_template_with_header(template_name_or_list, **kwargs):
    return render_template('header.html', TITLE = TITLE) + \
                render_template(template_name_or_list = template_name_or_list,
                    SUFFIX=SUFFIX, REDIR_INTRO = REDIR_INTRO,
                    TITLE = TITLE,
                    REDIR_HOME = REDIR_HOME,
                    **kwargs
                    )

# FUNCTIONS
# =========

#@memory.cache
def _query_metadata(md, query):
    md = query_rows(md, query = query)#.ix[:100,:]
   # if md.shape[0] == 0:
   #     md = pd.DataFrame([['No results found.']])
    #else:
        #md = md.sample(min(100, md.shape[0]), random_state=3)
    return md

def _usage_message(query):
    message = "="*20 + "\n\n" +\
        str(datetime.datetime.now()) + "\n" +\
        str(request.remote_addr) + "\n" +\
        str((inspect.stack()[1][3])) + "\n" +\
        query + "\n\n" + "="*20
    return message

# metadata_stats
# ==============

@memory.cache
def summarize_pairwise_wa():
    o = merge_data_frames(
        [
            pairwise_to_long(get_tax_dist(),
                        colnames=['taxon 1', 'taxon 2', 'tf-idf_cosine']),
            pairwise_to_long(get_tax_n_overlapping(),
                        colnames=['taxon 1', 'taxon 2', 'n_overlapping']),
            pairwise_to_long(get_tax_fisher(),
                        colnames=['taxon 1', 'taxon 2', 'fisher_exact_p-value']),
            pairwise_to_long(get_tax_corr(),
                        colnames=['taxon 1', 'taxon 2', 'correlation']),
            pairwise_to_long(get_tax_chisquare_kernel(),
                        colnames=['taxon 1', 'taxon 2', 'chisquare_kernel']),
        ],
        on = ['taxon 1', 'taxon 2']
        )
    return o


@memory.cache
def _biome_counts(sam_md):
    plt.figure(1, figsize = (5,5))
    k = sam_md.biome.value_counts().sort_values(ascending=False)[:20].reset_index()
    p = sns.barplot(x='biome', y='index', data=k,
                palette=sns.color_palette('colorblind', 10)[5:8],
                estimator = sum
                )
    p.set(xlabel='counts', ylabel='biome')
    img = BytesIO()
    plt.tight_layout()
    plt.savefig(img, table_format='png')
    img.seek(0)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    plt.close('all')
    return plot_url

def _dist_of_distance(D, entity='sample'):
    img = BytesIO()

    plt.figure(figsize = (5,5))
    #p = sns.distplot(D.sum())
    p = sns.distplot(D.values.flatten(), color = COLOURS[entity])
    p.set(xlabel='distance', ylabel='frequency')
    plt.tight_layout()

    plt.savefig(img, table_format='png')
    img.seek(0)
    plt.close()
    plt.close('all')
    return base64.b64encode(img.getvalue()).decode()


def _pairwise_dendrogram(Z, D):
    img = BytesIO()

    #sns.set(style="white")
    plt.figure(figsize=(10,4))
    den = dendrogram(Z, labels = D.index)#, truncate_mode='level', p=300) # abv_threshold_color='#AAAAAA')
    plt.xticks(rotation=90)
    plt.tight_layout()

    plt.savefig(img)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    plt.close('all')
    return plot_url

def _pairwise_samples_heatmap(abn, md, query):
    #TODO make it work
    img = BytesIO()

    tl.draw_clustermap_for_similar(query, abn, md)
    plt.tight_layout()

    plt.savefig(img)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    plt.close('all')
    return plot_url

def _plot2website(plot_func, **kwargs):
    # TODO: color by biome when plotting projects
    img = BytesIO()

    plot_func(**kwargs)

    plt.savefig(img)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    plt.close('all')
    return plot_url


#@memory.cache
def _pairwise_heatmap(D,use_index_level):
    # TODO: color by biome when plotting projects
    img = BytesIO()

    distance_matrix_heatmap(D, figsize=(10, 10), metric='cosine',
                    method='complete', use_index_level=use_index_level)
    #heatmap_with_levels(D, use_index_level=1, axis=0, figsize=(10, 10))#, metric='cosine', method='complete')#, use_index_level='biome')

    plt.savefig(img)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    plt.close('all')
    return plot_url

def _entity_ids_to_url(entity_ids, url_template):
    import urllib
    url_list = ['<a href="'+url_template.format(i)+'">{0}</a>'.format(i) for i in entity_ids]
    return url_list

def _table_insert_hyperlink(d, colname, url_template):
    #FIXME
    return d
    #d['a'] = d['a'].apply(lambda x: '<a href="https://www.ebi.ac.uk/metagenomics/projects/{0}">{0}</a>'.format(x))
    colname = flatten(d.columns.str.findall(colname, flags=re.I))
    #FIXME!!
    print('-'*80)
    print(colname)
    print('-'*80)
    colname = colname[0]
    if len(colname) == 1:
        colname = colname[0]
        d.is_copy = False
        s = '<a href="'+url_template+'">{0}</a>'
        print('-'*80)
        print(d.head())
        print('-'*80)
        print(colname)
        print('-'*80)
        d[colname] = d[colname].apply(lambda x: s.format(x))
        return d
    else:
        raise ValueError ('Colname not found or is not specific.')

def _taxon2species(tax_string):
    # TODO  not every entry has genus, species level. Should results be confined to only those that do?
    # e.g. collapse everything to genus level
    pass


def _table_format(d, reset_index=True, index_col_name = None, max_rows=MAX_ROW, **kwargs):
    '''
    d           A pandas DataFrame
    index_col_name  Name for the new column made from the index.
                If not provided, the name of index is used
    **kwargs    Other args to be passed to to_html()

    Returns:    A formatted string
    '''

    if reset_index:
        if index_col_name == None:
            if d.index.name == None:
                d.index.name = 'index'
        else:
            d.index.name = index_col_name

        if not d.index.name in d.columns:
            d.insert(0, d.index.name, names_to_human_readable(d.index))

    #if not d.shape[0] == 0:
        #d = d.ix[:min(max_rows, d.shape[0]), :]

    d.columns = names_to_human_readable(d.columns)
    d = d.to_html(index = False, escape=False, **kwargs)
    return d

def _1D_table_to_list():
    #TODO wrapped table?
    pass

def _table_stats(d, colnames):
    o = {}
    colnames = [c for c in colnames if c in d.columns]
    if len(colnames) == 0:
        o = pd.DataFrame()
    else:
        for c in colnames:
            o[c] = len(d[c].drop_duplicates())
        o = pd.DataFrame(pd.Series(o), columns=['counts'])
    return o

def available_entities(contrasts_directory):
    x = get_sam_md()
    ae = {}
    for i in ['project_id', 'sample_id', 'biome']:
        ae[i] = [j.upper() for j in list(set(x[i]).intersection(set(collM_c(ABN_FP, collapse_rows=i).index)))]
    ae['contrast_name'] = [os.path.splitext(i)[0].replace('differential_abundance_', '')
                for i in os.listdir(contrasts_directory) if os.path.splitext(i)[-1] == '.tsv']
    return ae



# =====
# VIEWS
# =====

@app.route('/hello/<name>')
def hello_name(name):
    print(css_style)
    return render_template('hello.html', name=name, css_style=css_style)


@app.route('/test')
def test():
    #return render_template('test.py.html')
    return render_template('test_flexbox.html')


@app.route('/intro', methods = ['POST', 'GET'])
def intro():
    return _render_template_with_header('simple_text.html',
                text = [TEXT.help_page]
                )


@app.route('/about', methods = ['POST', 'GET'])
def about():
    return 'TODO: ABOUT'

@app.route('/home', methods=['POST', 'GET'])
def home():
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    return _render_template_with_header('home.html',
                        page_title              = TITLE,
                        text_introduction       = TEXT.introduction,
                        redir_intro             = 'intro',
                        redir_metadata_stats    = 'metadata_stats',
                        redir_pairwise_samples  = 'samples/',
                        redir_pairwise_projects = 'projects/',
                        redir_pairwise_taxa     = 'taxa/',
                        redir_pairwise_contrasts= 'contrasts/',
                        )


@app.route("/metadata_table/results", methods=['POST', 'GET'])
def metadata_table():
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    if request.method == 'POST':
        descr_form = "Explanation of how query works."
        query = request.form['text'].strip().upper()
        print(_usage_message(query = query))

        sam_md = get_sam_md()

        data = _query_metadata(sam_md, query)

        if data.shape[0] == 0:
            print("=== Not found.")
            return _render_template_with_header('simple_text.html',
                        text = ['"'+query + '" not found in the public data.']
                        )

        else:
            table_stats = _table_stats(data, ['sample_id', 'project_id', 'biome'])

            data = _table_insert_hyperlink(data, colname='project_id',
                            url_template = URL_TEMPLATES['mnemo_projects'])
            data = _table_insert_hyperlink(data, colname='sample_id',
                            url_template = URL_TEMPLATES['mnemo_samples'])
            data = _table_insert_hyperlink(data, colname='biome',
                            url_template = URL_TEMPLATES['mnemo_biomes'])

            data                    = _table_format(data)
            table_stats             = _table_format(table_stats, index_col_name = 'entity')

            return _render_template_with_header('metadata_table.html',
                                query               = query,
                                descr_form          = descr_form,
                                tables              = [table_stats, data],
                                titles_tables       = ['', 'Table stats', 'Queried sample metadata'],
                                page_title          = "EBI Metagenomics sample information",
                                #redir_metadata_table= 'metadata_table/results',
                                form_redir_url      = url_for('metadata_table'),
                                )

@app.route("/metadata_table_amgut/results", methods=['POST', 'GET'])
def metadata_table_amgut():
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    if request.method == 'POST':
        descr_form = "Search the American Gut metadata and return the EBI metadata for hits."
        query = request.form['text'].strip().upper()
        print(_usage_message(query = query))

        amgut_md = get_amgut_md()

        #ATT
        #ss = _query_metadata(amgut_md, query).index
        #data = get_sam_md().loc[ss]
        #FIXME
        data = get_sam_md().sample(3)

        if data.shape[0] == 0:
            print("=== Not found.")
#            return _render_template_with_header('simple_text.html',
#                        text = ['"'+query + '" not found in the public data.']
#                        )
#
#        else:
        colnames_containing_query = amgut_md.columns[amgut_md.columns.str.contains(query, flags=re.I)]


        table_stats = _table_stats(data, ['sample_id', 'project_id', 'biome'])

        colnames_containing_query = _table_format(
                        pd.DataFrame(colnames_containing_query, columns=['Column name']).sort_values('Column name'))

        data = _table_insert_hyperlink(data, colname='project_id',
                        url_template = URL_TEMPLATES['mnemo_projects'])
        data = _table_insert_hyperlink(data, colname='sample_id',
                        url_template = URL_TEMPLATES['mnemo_samples'])
        data = _table_insert_hyperlink(data, colname='biome',
                        url_template = URL_TEMPLATES['mnemo_biomes'])

        data                    = _table_format(data)
        table_stats             = _table_format(table_stats, index_col_name = 'entity')

        return _render_template_with_header('metadata_table.html',
                            query               = query,
                            descr_form          = descr_form,
                            tables              = [table_stats, colnames_containing_query, amgut_md.to_html(), data],
                            titles_tables       = ['', 'Table stats', 'Column names containing query', 'Queried sample metadata'],
                            page_title          = "EBI Metagenomics sample information",
                            redir_metadata_table_amgut = 'metadata_table_amgut/results',
                            form_redir_url      = url_for('metadata_table_amgut'),
                            )
    else:
        return render_template('simple_text.html', text='Something went wrong.')

@app.route('/metadata_stats', methods=['POST', 'GET'])
@memory.cache
def metadata_stats():
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    # TODO use _usage_message
    sam_md = get_sam_md()
    plot_url1 = _biome_counts(sam_md)
    plot_url2 = _dist_of_distance(get_sam_dist(), 'sample')
    plot_url3 = _dist_of_distance(get_tax_dist(), 'taxon')


    d = _table_stats(sam_md, ['sample_id', 'project_id', 'biome'])
    d = _table_format(d)
    #descr_tables = ['', "Table description"]
    #descr_plots = ['', "Plot description"]
    titles_plots  = ['', 'Biome counts',
                    'Distribution of distances between samples',
                    'Distribution of distances between taxa'
                    ]
    descr_tables = ['']
    descr_plots = ['']


    t_samples = collM_c(ABN_FP).index
    t_biomes = list(set(sam_md.ix[t_samples].biome))
    text_list = ['', 'This edition of MNEMONIC includes '+str(len(t_samples))+\
                ' samples from biomes: '+', '.join(t_biomes)+'.',
                'But the "Metadata Stats" and "Biome counts" tables show data for all metadata.']

    plt.close('all')

    return _render_template_with_header( 'metadata_stats.html',
                            page_title    = "EBI Metagenomics stats",
                            text_list     = text_list,
                            tables        = [d],
                            titles_tables = ['', 'Metadata stats'],
                            plots         = [plot_url1, plot_url2, plot_url3],
                            titles_plots  = titles_plots,
                            descr_tables  = descr_tables,
                            descr_plots   = descr_plots,
                            )

@app.route("/pairwise_samples/results", methods=['POST', 'GET'])
def pairwise_samples():
    return 'TODO'

    '''
    sam_md = get_sam_md()
    query = request.form['text'].split().upper()
    print(_usage_message(query = query))

    if not query in sam_md['sample_id']:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.'])

    # broken
    # abn = collM_c()
    #abn = add_index_level(abn, SAM, 'sample_id', 'project_id')
    #plot_url = _pairwise_samples_heatmap(abn, sam_md, query = "ERP010458")
    descr_tables = "Projects most similar to "+str(query)+"."

    #D = _current_raw_to_dist(abn)
    #d = tl.similar_projects(query, D, sam_md)
    # TODO this does not sort /select the prjs in any fashion!
    d = pd.DataFrame()

    return _render_template_with_header( 'metadata_stats.html',
                            tables        =  [d.to_html()],
                            #titles_tables = ['', 'Similar projects'],
                            titles_tables = ['', 'CURRENTLY DOWN'],
                            #plots         = [plot_url],
                            #titles_plots  = ['', 'Biome counts'],
                            descr_tables = ['', descr_tables],
                            #descr_form = descr_form,
                            page_title="Pairwise relationship results for samples",
                            text_list     = [],
                            )
    '''


# ============
# ENTITY PAGES
# ============
def pairwise_page_metadata():
    pass

def get_entity_info(entity_label, entity_labels_sam = ENTITY_LABELS_SAM, entity_labels_tax = ENTITY_LABELS_TAX):
    if entity_label in entity_labels_sam:
        primary_entity_colname  = 'sample_id'
        primary_entity_label    = 'sample'

        if entity_label == 'sample':
            entity_colname          = 'sample_id'
            url_template            = URL_TEMPLATES['mnemo_samples']
            external_links_template = [URL_TEMPLATES['ebi_samples']]
        elif entity_label == 'project':
            entity_colname          = 'project_id'
            url_template            = URL_TEMPLATES['mnemo_projects']
            external_links_template = [URL_TEMPLATES['ebi_projects']]
        elif entity_label == 'biome':
            entity_colname          = 'biome'
            url_template            = URL_TEMPLATES['mnemo_biomes']
            external_links_template = [URL_TEMPLATES['ebi_projects_search']]

    elif entity_label in entity_labels_tax:
        primary_entity_colname  = 'taxonomy' #maybe otu? woudl be better
        primary_entity_label    = 'taxonomy'

        if entity_label == 'taxonomy':
            entity_colname          = 'taxonomy'
            url_template            = URL_TEMPLATES['mnemo_taxa']
            external_links_template = []
        elif entity_label == 'family':
            entity_colname          = 'family'
            url_template            = URL_TEMPLATES['mnemo_families']
            external_links_template = []
    else:
        raise ValueError('Entity label not recognized.')
    o = {'colname': entity_colname, 'pe_colname': primary_entity_colname,
                'pe_label': primary_entity_label, 'url_template': url_template,
                'external_links_template': external_links_template}
    return o
    # WARNING: messing up order will stil break some things
    #return entity_colname, primary_entity_colname, primary_entity_label, url_template

def pairwise_page_tables_plots(entity_id, entity_label, draw_dendro=True):
    '''
    To be called inside entity-specific fun
    Returns
    plots, titles_plots, descr_plots, tables, titles_tables, descr_tables
    '''
    # for taxa - transpose
    o = ["plots", "titles_plots", "descr_plots", "tables", "titles_tables", "descr_tables"]
    o = {i:[] for i in o}

    entity_colname, primary_entity_colname, primary_entity_label, url_template =\
                [get_entity_info(entity_label)[i] for i in ['colname', 'pe_colname', 'pe_label', 'url_template']]

    ### TODO replace this with somethign more sensible (matching with re.I flag?)
    if entity_label == 'biome': #TODO which else?
        entity_id = entity_id.capitalize()
    ###

    metric_label = 'metric'

    print(_usage_message(query = entity_id))
    md = get_sam_md()

    ### PAIRWISE
    D = get_sam_dist()
    D = collM_c(D, collapse_rows=entity_colname, collapse_cols=entity_colname,
                    primary_levels=(primary_entity_colname, primary_entity_colname), method=np.mean)
    #D.index.name = None
    D.index.name = None
    D.columns.name = None
    table1 = pairwise_to_long(D, colnames=[entity_label+' 1', entity_label+' 2', metric_label])
    table1.index = table1.index.set_names("sample_id")
    table1 = table1.ix[[i == entity_id for i in table1[entity_label+' 1']]]\
                    .sort_values(metric_label).iloc[:min(MAX_ROW, table1.shape[0]), :]
    table1 = table1[[entity_label+' 2', metric_label]]

    ps = set([entity_id] + table1[entity_label+' 2'][:min(20, table1.shape[0])].tolist())


    table1 = _table_insert_hyperlink(table1, colname=entity_label+' 2',
                        url_template = url_template)

    # table1       = _table_format(table1)
    # Why not working TODO
    table1.columns = names_to_human_readable(table1.columns)
    table1 = table1.to_html(index = False, escape=False)

    o['tables']          = [table1]
    o['titles_tables']   = ['', pluralize(entity_label.capitalize())+" most similar to "+str(entity_id)]
    o['descr_tables']    = ['',
                            'For agglomerative entities like project, "Metric" is the mean distance\
                            from each low level entity (e.g. sample) in the query entity (e.g. project)\
                            to each low-level entity in the other high-level entity.'
                            ]
    #FIXME
    return o

    if draw_dendro:
        D_subset =  D.ix[ps,ps]
        Zp = linkage(D_subset, method = 'complete')

        #TODO shows a warning fr sample
        #site_main.py:631: ClusterWarning: scipy.cluster: The symmetric non-negative hollow obs
        #observation matrix looks suspiciously like an uncondensed distance matrix

        plot_url1 = _pairwise_dendrogram(Zp, D_subset)
        if entity_label == 'sample':
            D_subset = add_index_level(D_subset, mapping=md,
                            new_level='project_id', existing_level=entity_colname, axis='both')
            plot_url2 = _pairwise_heatmap(D_subset, use_index_level='project_id')
        elif entity_label == 'taxonomy':
            plot_url2 = "not implemented"
        elif entity_label == 'biome':
            # don't use any coloring (colour by biome)
            plot_url2 = _pairwise_heatmap(D_subset, use_index_level='biome')
        else:
            D_subset = add_index_level(D_subset, mapping=md,
                            new_level='biome', existing_level=entity_colname, axis='both')
            plot_url2 = _pairwise_heatmap(D_subset, use_index_level='biome')

        o['plots']           = [plot_url1, plot_url2]
        o['titles_plots']    = ['',
                                    pluralize(entity_label.capitalize())+' dendrogram for '+str(entity_id),
                                    pluralize(entity_label.capitalize())+' heatmap for '+str(entity_id),
                                    ]
    else:
        o['descr_plots']      = []
        o['plots']            = []
        o['titles_plots']     = []

    del D
    plt.close('all')
    return o


def format_external_links(entity_id, entity_label):
    return [i.format(entity_id) for i in get_entity_info(entity_label)['external_links_template']]


# SAMPLES
# -------

@app.route('/samples/redirect', methods=['GET', 'POST'])
def samples_redirect():
    query = request.form['text'].strip().upper()

    sam_md = get_sam_md()
    print(_usage_message(query = query))
    if not query in AVAILABLE_ENTITIES['sample_id']: #sam_md.project_id:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            #'Try "'+str(sam_md.project_id.sample(1).values[0])+'" instead?'])
                            'Try "'+AVAILABLE_ENTITIES['sample_id'][np.random.randint(len(AVAILABLE_ENTITIES['sample_id']))]+'" instead?'])
    else:
        return redirect(url_for('show_sample', sample_id=query))


@app.route('/samples/<sample_id>', methods=['GET', 'POST'])
def show_sample(sample_id):
    entity_label = 'sample'

    query = str(sample_id).strip().upper()
    print(_usage_message(query = query))
    sam_md = get_sam_md()

    if not query in AVAILABLE_ENTITIES['sample_id']: #sam_md.project_id:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            #'Try "'+str(sam_md.project_id.sample(1).values[0])+'" instead?'])
                            'Try "'+AVAILABLE_ENTITIES['sample_id'][np.random.randint(len(AVAILABLE_ENTITIES['sample_id']))]+'" instead?'])

    md_entry = sam_md.ix[[ i.upper()  == query for i in sam_md[get_entity_info(entity_label)['colname']] ]]

    #ls_samples = md_entry[['sample_id']].drop_duplicates()
    md_entry.is_copy = False
    ##d = d[[i for i in usecols if i in d.columns]]
    md_entry.set_index('sample_id', drop=True, inplace=True)
    #md_entry.drop(['sample_id', 'sample_name'], 1, inplace=True)
    md_entry.drop_duplicates(inplace=True)

    ## TODO
    md_entry      = _table_insert_hyperlink(md_entry, colname = 'project_id',
                        url_template  = URL_TEMPLATES['mnemo_projects'])
    md_entry      = _table_insert_hyperlink(md_entry, colname = 'biome',
                        url_template  = URL_TEMPLATES['mnemo_biomes'])
    md_entry      = _table_format(md_entry)
    #ls_samples    = _table_format(ls_samples.ix[:MAX_ROW, :])


    tables_plots = pairwise_page_tables_plots(query, entity_label)
    tables_plots['tables'] = [md_entry] + tables_plots['tables']
    tables_plots['titles_tables'] = tables_plots['titles_tables'][0:1] +\
                        ['Sample information'] + tables_plots['titles_tables'][1:]
    external_links = format_external_links(query, entity_label)

    return _render_template_with_header( 'entity_page.html',
                            entity_id        = query,
                            entity_label     = entity_label,
                            entity_label_pl  = pluralize(entity_label),
                            external_links   = external_links,
                            **tables_plots,
                            )


@app.route('/projects/redirect', methods=['GET', 'POST'])
def projects_redirect():
    query = request.form['text'].strip().upper()

    sam_md = get_sam_md()
    print(_usage_message(query = query))
    if not query in AVAILABLE_ENTITIES['project_id']: #sam_md.project_id:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            #'Try "'+str(sam_md.project_id.sample(1).values[0])+'" instead?'])
                            'Try "'+AVAILABLE_ENTITIES['project_id'][np.random.randint(len(AVAILABLE_ENTITIES['project_id']))]+'" instead?'])
    else:
        return redirect(url_for('show_project', project_id=query))

@app.route('/projects/<project_id>', methods=['GET', 'POST'])
def show_project(project_id):
    # TODO don't show table, put elements in html segments
    entity_label = 'project'
    query = str(project_id).strip().upper()
    print(_usage_message(query = query))
    sam_md = get_sam_md()

    if not query in AVAILABLE_ENTITIES['project_id']: #sam_md.project_id:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            #'Try "'+str(sam_md.project_id.sample(1).values[0])+'" instead?'])
                            'Try "'+AVAILABLE_ENTITIES['project_id'][np.random.randint(len(AVAILABLE_ENTITIES['project_id']))]+'" instead?'])

    md_entry = sam_md.ix[[ i.upper()  == query for i in sam_md[get_entity_info(entity_label)['colname']] ]]

    ls_samples = md_entry[['sample_id']].drop_duplicates()
    md_entry.is_copy = False
    #d = d[[i for i in usecols if i in d.columns]]
    md_entry.set_index('project_id', drop=True, inplace=True)
    md_entry.drop(['sample_id', 'sample_name'], 1, inplace=True)
    md_entry.drop_duplicates(inplace=True)

    md_entry    = _table_insert_hyperlink(md_entry, colname = 'biome',
                                        url_template  = URL_TEMPLATES['mnemo_biomes'])
    md_entry      = _table_format(md_entry)
    ls_samples    = _table_insert_hyperlink(ls_samples, colname = 'sample_id',
                                        url_template  = URL_TEMPLATES['mnemo_samples'])
    ls_samples    = _table_format(ls_samples.ix[:MAX_ROW, :])


    tables_plots = pairwise_page_tables_plots(query, entity_label=entity_label)
    tables_plots['tables'] = [md_entry, ls_samples] + tables_plots['tables']
    tables_plots['titles_tables'] = tables_plots['titles_tables'][0:1] +\
                        ['Project information', 'List of samples in project (truncated)'] + tables_plots['titles_tables'][1:]
    tables_plots['descr_tables'] = tables_plots['descr_tables'][0:1] +\
                        ['', ''] + tables_plots['descr_tables'][1:]
    external_links = format_external_links(query, entity_label)


    return _render_template_with_header( 'entity_page.html',
                            entity_id       = query,
                            entity_label    = entity_label,
                            entity_label_pl = pluralize(entity_label),
                            external_links  = external_links,
                            **tables_plots,
                            )


@app.route('/biomes/redirect', methods=['GET', 'POST'])
def biomes_redirect():
    query = request.form['text'].strip().upper()

    sam_md = get_sam_md()
    print(_usage_message(query = query))

    if not query in AVAILABLE_ENTITIES['biome']: #sam_md.project_id:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            'Try "'+AVAILABLE_ENTITIES['biome'][np.random.randint(len(AVAILABLE_ENTITIES['biome']))]+'" instead?'])

    else:
        return redirect(url_for('show_biome', biome=query))


@app.route('/biomes/<biome>', methods=['GET', 'POST'])
def show_biome(biome):

    # TODO don't show table, put elements in html segments
    entity_label = 'biome'
    query = str(biome).strip().upper()
    print(_usage_message(query = query))
    sam_md = get_sam_md()

    if not query in AVAILABLE_ENTITIES['biome']: #sam_md.project_id:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            'Try "'+AVAILABLE_ENTITIES['biome'][np.random.randint(len(AVAILABLE_ENTITIES['biome']))]+'" instead?'])

    md_entry = sam_md.ix[[ i.upper()  == query for i in sam_md[get_entity_info(entity_label)['colname']] ]]

    ls_samples = md_entry[['project_id']].drop_duplicates()
    #md_entry.is_copy = False
    md_entry = 'TODO'
    #md_entry.set_index('biome', drop=True, inplace=True)
    #md_entry.drop(['sample_id', 'sample_name'], 1, inplace=True)
    #md_entry.drop_duplicates(inplace=True)

    #md_entry      = _table_format(md_entry)
    ls_samples    = _table_insert_hyperlink(ls_samples, colname = 'project_id',
                                url_template  = URL_TEMPLATES['mnemo_projects'])
    ls_samples    = _table_format(ls_samples.ix[:MAX_ROW, :])


    tables_plots = pairwise_page_tables_plots(query, entity_label=entity_label)
    tables_plots['tables'] = [md_entry, ls_samples] + tables_plots['tables']
    tables_plots['titles_tables'] = tables_plots['titles_tables'][0:1] +\
                        ['Biome information', 'List of projects in biome (truncated)'] + tables_plots['titles_tables'][1:]
    tables_plots['descr_tables'] = tables_plots['descr_tables'][0:1] +\
                        ['', ''] + tables_plots['descr_tables'][1:]
    external_links = format_external_links(query, entity_label)

    return _render_template_with_header( 'entity_page.html',
                            entity_id       = query,
                            entity_label    = entity_label,
                            entity_label_pl = pluralize(entity_label),
                            external_links  = external_links,
                            **tables_plots,
                            )

# TAXA
# ----

@app.route('/taxa/redirect', methods=['GET', 'POST'])
def taxa_redirect():
    #disambiguate
    # TODO do some sort of mathing for entires like C.difficile

    original_query = request.form['text'].strip().upper()
    query_taxon = request.form['text'].strip().upper()
    print(_usage_message(query = query_taxon))
    abn = collM_c(ABN_FP )
    query_taxon = abn.T.index[abn.T.index.str.contains(query_taxon, flags = re.I)]
    #tax_md = get_tax_md()
    #query_taxon = tax_md.index[tax_md.index.str.contains(query_taxon, flags = re.I)]

    if len(query_taxon) == 0:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+original_query + '" not found in the public data.'])
    #elif len(query_taxon) == 1:
        #return redirect(url_for('show_taxon', taxon_id=query_taxon))

    else:
        url_list = ['Your query matches the following taxonomic entities:<br>'] +\
                    _entity_ids_to_url(query_taxon, url_template = URL_TEMPLATES['mnemo_taxa'])

        return _render_template_with_header('simple_text.html',
                                    text = url_list,
                                    )



@app.route('/taxa/<taxon_id>', methods=['GET', 'POST'])
def show_taxon(taxon_id):

    print(_usage_message(query = taxon_id))
    tables = []

#    # TODO: don't add multilevel. Return species level
#    # TODO add url but display genus + species name

    ### these need to be consistent
    metrics = {'tf-idf_cosine':True, 'n_overlapping':False, 'correlation':False,
                'fisher_exact_p-value':True, 'chisquare_kernel': True}
    d = summarize_pairwise_wa()
    ###

    titles_tables    = ['']
    download_tables  = {}
    descr_tables     = ['']

    plots            = []
    titles_plots     = ['']
    descr_plots      = []

    d = d.ix[d['taxon 1'] == taxon_id]
    download_tables['taxon_relationships.tsv'] = d
    store_files(download_tables)
    d.drop('taxon 1', 1, inplace=True)

    for metric, ascending in metrics.items():
        table_main = d.sort_values(metric, ascending=ascending)

        table = _table_insert_hyperlink(table_main.iloc[:20,], colname='taxon 2',
                url_template = URL_TEMPLATES['mnemo_taxa'])
        table = _table_format(table, reset_index=False)
        tables.append(table)
        titles_tables.append(names_to_human_readable(metric)+"; ascending="+str(ascending))

        table = _table_insert_hyperlink(table_main.iloc[-20:,], colname='taxon 2',
                url_template = URL_TEMPLATES['mnemo_taxa'])
        table = _table_format(table, reset_index=False)
        tables.append(table)
        titles_tables.append(names_to_human_readable(metric)+"; ascending="+str(not ascending))


    return _render_template_with_header( 'metadata_stats.html',
                            page_title      = taxon_id,
                            text_list       = [],
                            tables          = tables,
                            titles_tables   = titles_tables,
                            descr_tables    = descr_tables,
                            plots           = plots,
                            titles_plots    = titles_plots,
                            descr_plots     = descr_plots,
                            #descr_form     = descr_form,
                            download_tables_filenames = list(download_tables.keys()),
                            )


### contrasts
@app.route('/contrasts/redirect', methods=['GET', 'POST'])
def contrasts_redirect():
    query = request.form['text'].strip()#.upper() #for now

    print(_usage_message(query = query))

    query_matching = [i for i in AVAILABLE_ENTITIES['contrast_name'] if i.find(query) >0] #i.upper()

    # TODO list of files in the diff analysis flder
    if query == "":
        url_list = ["Here's a list of all precomputed contrasts:<br>"] +\
                    _entity_ids_to_url(
                            AVAILABLE_ENTITIES['contrast_name'],
                            url_template = URL_TEMPLATES['mnemo_contrasts'])

        return _render_template_with_header('simple_text.html',
                                    text = url_list,
                                    )



    if len(query_matching) == 0:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            'Try "'+AVAILABLE_ENTITIES['contrast_name']\
                            [np.random.randint(len(AVAILABLE_ENTITIES['contrast_name']))]+'" instead?'])

    else:
        url_list = ['Your query matches the following contrasts:<br>'] +\
                    _entity_ids_to_url(query_matching, url_template = URL_TEMPLATES['mnemo_contrasts'])

        return _render_template_with_header('simple_text.html',
                                    text = url_list,
                                    )


@app.route('/contrasts/<contrast>', methods=['GET', 'POST'])
def show_contrast(contrast):
    contrast_fp = os.listdir(contrasts_directory)
    contrast_fp = [i for i in contrast_fp if i.find(contrast) > 0] #ATT maybe ther are more that match?

    entity_label = 'contrast'
    query = contrast#str(contrast).strip().upper()
    print(_usage_message(query = query))

    if len(contrast_fp) == 0:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the local file storage. I screwed up, sorry.',
                            'Why not try "'+AVAILABLE_ENTITIES['contrast_name'][np.random.randint(len(AVAILABLE_ENTITIES['contrast_name']))]+'" instead?'])
    else:
        contrast_fp = contrasts_directory+contrast_fp[0]


    if not query in AVAILABLE_ENTITIES['contrast_name']:
        return _render_template_with_header('simple_text.html',
                    text = ['"'+query + '" not found in the public data.',
                            'Try "'+AVAILABLE_ENTITIES['contrast_name'][np.random.randint(len(AVAILABLE_ENTITIES['contrast_name']))]+'" instead?'])
    #query = str(contrast).strip().upper()

    #TODO get a list of samples too
    #TODO make it possible to upload a file with sample lists (table format)

    #sam_md = get_sam_md()
    da = pd.read_csv(contrast_fp, sep='\t', index_col=0)

    tables_plots = ["plots", "titles_plots", "descr_plots", "tables", "titles_tables", "descr_tables"]
    tables_plots = {i:[] for i in tables_plots}

    #tables_plots['tables'] = [table.head().to_html() for table in [da]]
    #tables_plots['titles_tables'] = ['', 'Test']
    ### PLOTS
            #plot_url2 = _pairwise_heatmap(D_subset, use_index_level='biome')
            #o['plots']           = [plot_url1, plot_url2]
    plot_contrasts = tuple(contrast.replace('_', ' ').split('-'))
    plot1 = _plot2website(plot_log2fc_global, deseq2_table = da, contrasts = plot_contrasts)
    plot2 = _plot2website(plot_log2fc,        deseq2_table = da, contrasts = plot_contrasts, show_n=14)
    plot3 = _plot2website(plot_MA,            deseq2_table = da, contrasts = plot_contrasts)
    plot4 = _plot2website(plot_volcano,       deseq2_table = da, contrasts = plot_contrasts)
    plot5 = _plot2website(plot_stderr,        deseq2_table = da, contrasts = plot_contrasts)

    tables_plots['plots'] = [plot1, plot2, plot3, plot4, plot5]
    tables_plots['descr_plots'] = []

    external_links = []#format_external_links(query, entity_label)

    return _render_template_with_header( 'entity_page.html',
                            entity_id       = query,
                            entity_label    = entity_label,
                            entity_label_pl = pluralize(entity_label),
                            external_links  = external_links,
                            **tables_plots,
                            )

#@app.route('/metacontrast/<metacontrast_id>', methods=['GET', 'POST'])
@app.route('/metacontrast', methods=['GET', 'POST'])
def show_metacontrast(metacontrast_id=None):

    print(_usage_message(query = str(metacontrast_id)))
    tables = []

    titles_tables    = ['']
    download_tables  = {}
    descr_tables     = ['']

    plots            = []
    titles_plots     = ['']
    descr_plots      = []

    #TODO heatmap
    lfc_matrix = mdiff.summarize(da_resdir = contrasts_directory)


    metacontrast_fp = '/D/differential_comparison.tsv'
    d = pd.read_csv(metacontrast_fp, sep='\t', index_col=0)

    c=0
    for c1,c2 in itertools.combinations(d.columns, 2):
        c+=1
        p = _plot2website(sns.regplot, x=d[c1], y=d[c2])
        plots.append(p)
        if c>10:
            break


    return _render_template_with_header( 'metadata_stats.html',
                            page_title      = 'contr comp',
                            text_list       = [],
                            tables          = tables,
                            titles_tables   = titles_tables,
                            descr_tables    = descr_tables,
                            plots           = plots,
                            titles_plots    = titles_plots,
                            descr_plots     = descr_plots,
                            #descr_form     = descr_form,
                            download_tables_filenames = list(download_tables.keys()),
                            )
def store_files(tables, file_storage_fp = FILE_STORAGE):
    '''
    tables : a dict filename:table
    '''
    for filename in os.listdir(file_storage_fp):
        os.remove(file_storage_fp+"/"+filename)
    for filename, table in tables.items():
        table.to_csv(file_storage_fp+"/"+filename, sep='\t', index=False)



@app.route('/download_file/<filename>', methods=['GET', 'POST'])
def download_file(filename, file_storage_fp = FILE_STORAGE):
    #buff = BytesIO()
    #buff.write(table.to_csv(sep='\t', index=False).encode('utf-8'))
    #buff.seek(0)
    #return send_file(   buff,
    #                    mimetype='text/csv',
    #                    attachment_filename=filename,
    #                    as_attachment=True,
    #                    )
    filename = FILE_STORAGE+"/"+filename
    return send_file(filename, attachment_filename = filename)

@memory.cache
def initiate():
    pass

if __name__ == "__main__":
    #initiate()
    populate_cache()
    # TODO this is not very elegant
    contrasts_directory = '/P/mnemonic-old/differential_analysis_results/'
    AVAILABLE_ENTITIES = available_entities(contrasts_directory)
    #print(AVAILABLE_ENTITIES)
    print('Env ready')
    print('Running on '+str(HOST)+":"+str(PORT))
    app.run(host=HOST, port=PORT, debug=ISDEV)

